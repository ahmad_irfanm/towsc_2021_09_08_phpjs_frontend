import Vue from 'vue'
import VueRouter from "vue-router";
import Homepage from "./views/Homepage";
import NotFound from "./views/NotFound";
import EventDetail from "./views/EventDetail";
import SessionDetail from "./views/SessionDetail";
import EventRegistration from "./views/EventRegistration";
import Login from "./views/Login";

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        component: Homepage,
    },
    {
        path: '/organizers/:organizerSlug/events/:eventSlug',
        component: EventDetail,
    },
    {
        path: '/organizers/:organizerSlug/events/:eventSlug/sessions/:sessionId/:sessionSlug',
        component: SessionDetail,
    },
    {
        path: '/organizers/:organizerSlug/events/:eventSlug/registration',
        component: EventRegistration,
    },
    {
        path: '/login',
        component: Login,
    },
    {
        path: '*',
        component: NotFound,
    }
]

const router = new VueRouter({ routes })

export default router